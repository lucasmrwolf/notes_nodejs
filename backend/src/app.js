const express = require('express')
const cors = require('cors')

const routes = require('./routers/notesRoutes')
const database = require('./config/database')

const app = express()

app.use(express.urlencoded({extended: true,}));
app.use(express.json())

app.use(cors())
app.listen(3002)
app.use(routes)

console.log(new Date().toLocaleString() + " - Listening port 3002")
database.connection;


