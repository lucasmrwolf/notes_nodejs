const express = require('express')
const routes = express.Router()
const NotesController = require('../controllers/notesController')

routes.get("/notes", NotesController.read)
routes.post("/newnote", NotesController.create)
routes.post("/updatenote/:id", NotesController.update)
routes.delete("/deletenote/:id", NotesController.delete)

module.exports = routes