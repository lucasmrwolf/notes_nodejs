let Connection = require('tedious').Connection;  
let Request = require('tedious').Request
    var config = {  
        server: 'localhost', 
        authentication: {
            type: 'default',
            options: {
                userName: 'sa', 
                password: 'neo@123'  
            }
        },
        options: {
            encrypt: true,
            database: 'notes' 
        }
};  
let connection = new Connection(config);  
connection.on('connect', function(error) { 
    if(error){
        console.log(new Date().toLocaleString() + "- Error to connected with database: " + error);  
    }else{
    console.log(new Date().toLocaleString() + "- Connected with Database sucefully!");  
    }
});
connection.connect();

function execQuery(sqlQuery){
    const result = [];

    return new Promise((resolve, reject) =>{
        const request = new Request(sqlQuery, statementComplete);

        request.on('row', async function(columns){
            const entries = {};
            columns.forEach((column) => {
                entries[column.metadata.colName] = column.value;
            });
            result.push(entries);
        });
        
        request.on('doneProc', function (rowCount, more, returnStatus, rows) {
            return resolve(result);
        });

        connection.execSql(request);
    });
}; 

function statementComplete(error, rowCount) {
    if (error) {
      console.log('Statement failed: ' + error);
    } else {
      console.log(rowCount + ' rows returned.');
    }
  }

module.exports = {
    connection, execQuery
}