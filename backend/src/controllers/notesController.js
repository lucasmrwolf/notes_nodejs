const Projects = require('../models/notesModel')
const connectionDB = require('../config/database')

module.exports = {

    async read(request, response){
        const notes_list = await connectionDB.execQuery("select * from notes");
        return response.json(notes_list)
    },

    async create(request, response){
        const {title, description} = request.body
        const projectCreated = await connectionDB.execQuery("insert into notes(title, description) values('"+ title +"', '"+ description +"')");
        if(projectCreated){
            return response.json({"message":"Nota criada com sucesso!"})
        }
        return response.json({"error":"Ocorreu um problema ao criar o projeto!"})
    },

    async delete(request, response){
        const {id} = request.params
        const projectDeleted = await connectionDB.execQuery("delete notes where id='"+id+"'"); 
        if (projectDeleted){
            return response.json({"message":"Projeto deletado com sucesso!"})
        }
        return response.json({"error":"Ocorreu um problema ao deletar o projeto!"})
    },

    async update(request, response){
        const {id} = request.params
        const {title, description} = request.body
        const projectUpdated = await Projects.findOne({_id:id})
        if(title){
            projectUpdated.title = title
        }
        if(description){
            projectUpdated.description = description
        }
        if(projectUpdated){
            await projectUpdated.save()
            return response.json({"message": "Projeto alterado com sucesso!"})
        }
        return response.json({"error":"Ocorreu um erro ao tentar alterar o projeto!"})
    }
}