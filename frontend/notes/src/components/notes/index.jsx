

function Notes({data}){
    return(
        <div id="Notes">
            <div className="card">
                <div className="card-body">
                    <div className="card-title">
                        <h4>{data.title}</h4>
                    </div>
                    <p>{data.description}</p>
                </div>
            </div>
        </div>
    )
}

export default Notes;