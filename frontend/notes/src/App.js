import React, { useState, useEffect } from "react";
import Notes from './components/notes';
import NavBar from './components/navbar';
import api from './services/api'

function App() {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [notes, setNotes] = useState([]);

  useEffect(() => {
    async function getNotes() {
      const response = await api.get("/notes",);
      setNotes(response.data);
    }
    getNotes();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const response = await api.post("/newnote", { title, description });
    setTitle('');
    setDescription('');
    setNotes([...notes, response.data])
  }





  return (
    <div id="App">
      <NavBar></NavBar>
      <div className="container">
        <div className="row">
          <div className="col-4">notas indexadas</div>
          <div className="col-4">
            <div class="card">
              <div class="card-header">
                <p>Nova nota</p>
              </div>
              <form onSubmit={handleSubmit}>
                <div className="card-body">
                  <div className="card-title">
                    <input type="text" className="form-control" required htmlFor="title" value={title} onChange={e => setTitle(e.target.value)} placeholder="Digite o titulo da nota" />
                  </div>
                  <div className="card-text">
                    <textarea className="form-control" htmlFor="description" required value={description} onChange={e => setDescription(e.target.value)} placeholder="Descrição" rows="3"></textarea>
                  </div>
                </div>
                <div class="card-footer">
                  <button type="submit" className="btn btn-primary">Criar Nota</button>
                </div>
              </form>
            </div>
          </div>
          <div className="col-4">notas indexadas</div>
        </div>
        <div className="row">
          <br></br>
          <div className="row">
            {notes.map(data => (

              <div className="col-2">
                <Notes data={data}></Notes>
              </div>
            ))}
          </div>

        </div>
      </div>
    </div>
  );
}

export default App;
